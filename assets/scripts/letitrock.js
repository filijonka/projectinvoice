var $contactHolder;
var $addContactLink = $('<button class="btn btn-primary add_tag_link">Add a contact</button>');
var $newContactLinkLi = $('<li class="form-group col-md-12"></li>').append($addContactLink);

function addTagForm($collectionHolder, $newLinkLi, $class) {
  // Get the data-prototype explained earlier
  var prototype = $collectionHolder.data('prototype');
  //get the startindex based on numbers of images already added
  var startIndex = $('.js-images div.js-image').length;
  // get the new index
  var index = $collectionHolder.data('index');

  if (index <= startIndex) {
    index = startIndex+1;
  }

  // Replace '__name__' in the prototype's HTML to
  // instead be a number based on how many items we have
  var newForm = prototype.replace(/__name__/g, index);

  // increase the index with one for the next item
  $collectionHolder.data('index', index + 1);

  // Display the form in the page in an li, before the "Add a tag" link li
  var $newFormLi = $('<li class="form-group ' + $class +'"></li>').append(newForm);
  $newLinkLi.before($newFormLi);
}


$(document).ready(function() {

  // Get the ul that holds the collection of contacts
  $contactHolder = $('ul.js-contacts');

  // add the "add a tag" anchor and li to the tags ul
  $contactHolder.append($newContactLinkLi);

  // count the current form inputs we have (e.g. 2), use that as the new
  // index when inserting a new item (e.g. 2)
  $contactHolder.data('index', $contactHolder.find(':input').length);

  $addContactLink.on('click', function(e) {
    // prevent the link from creating a "#" on the URL
    e.preventDefault();

    // add a new tag form (see next code block)
    addTagForm($contactHolder, $newContactLinkLi,"col-md-4 new-tag");
  });

  $('.js-remove-contact-link').on("click", function(e) {
    e.preventDefault();
    var values = JSON.parse($(this).val());

    $.ajax({
      type: 'POST',
      url: '/ajaxRoute/removeContact',
      data: values,
      dataType: "json",
      success: function (data) {
        if (data.success) {
          $('button[name="' + data.element + '"]').parent().parent().remove();
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 404) {
          //No ticket found
          console.log('404')
        }
        else {
          console.log('failure', errorThrown);
        }
      }
    });
  });

  $('.js-search-customer input').autocomplete({
    minLength: 1,
    source: function (request, response) {
      $.ajax({
        type: 'POST',
        url: '/ajaxRoute/findCustomer',
        data: {"customer": request.term},
        dataType: "json",
        success: function (data) {
          response(data.customers);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          if (jqXHR.status === 404) {
            //No ticket found
            console.log('404')
          }
          else {
            console.log('failure', errorThrown);
          }
        }
      });
    }
  });
  
  $('.js-search-project input').autocomplete({
    minLength: 1,
    source: function (request, response) {
      $.ajax({
        type: 'POST',
        url: '/ajaxRoute/findProject',
        data: {"project": request.term},
        dataType: "json",
        success: function (data) {
          response(data.projects);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          if (jqXHR.status === 404) {
            //No ticket found
            console.log('404')
          }
          else {
            console.log('failure', errorThrown);
          }
        }
      });
    }
  });

  $('.js-create-invoice-base').on('click', function (e) {
    e.preventDefault();
    var timeReportIds = [];
    $('input[type=checkbox]').each(function () {
      var value = (this.checked ? $(this).val() : "");
      if (value !== '') {
        timeReportIds.push(value);
      }
    });

    $.ajax({
      type: 'POST',
      url: '/ajaxRoute/creteInvoiceBase',
      data: {"input": timeReportIds},
      dataType: "json",
      success: function (data) {
        window.location = data.url;
      },
      error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 404) {
          console.log('404')
        }
        else {
          console.log('failure', errorThrown);
        }
      }
    });
  });
  
  $('.js-create-invoice').on('click', function (e) {
    e.preventDefault();
    var invoiceBaseIds = [];
    $('input[type=checkbox]').each(function () {
      var value = (this.checked ? $(this).val() : "");
      if (value !== '') {
        invoiceBaseIds.push(value);
      }
    });

    var invoice = $('select.js-select-invoice').val();

    $.ajax({
      type: 'POST',
      url: '/ajaxRoute/createInvoice',
      data: {"invoiceBaseIds": invoiceBaseIds, "invoice":invoice},
      dataType: "json",
      success: function (data) {
        window.location = data.url;
      },
      error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 404) {
          console.log('404')
        }
        else {
          console.log('failure', errorThrown);
        }
      }
    });
  });

  $('.js-set-paid').on('click', function (e) {
    e.preventDefault();
    $.ajax({
      type: 'POST',
      url: '/ajaxRoute/setInvoicePaid',
      data:{"invoiceId": $(this).attr('href')},
      dataType: "json",
      success: function (data) {
        location.reload();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 404) {
          console.log('404')
        }
        else {
          console.log('failure', errorThrown);
        }
      }
    });
  });
  
  
  $('a.collapsed').click(function () {
    var selector = '.' + $(this).attr('rel');
    $(selector).toggle();
    if ($(this).hasClass('expanded')) {
      $(this).removeClass('expanded');
    } else {
      $(this).addClass('expanded');
    }

    return false;
  });
  
  
});
