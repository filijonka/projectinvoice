<?php

namespace Deployer;

require 'recipe/symfony3.php';

// Configuration

set('repository', 'git@bitbucket.org:letitrock/');
set('git_tty', true); // [Optional] Allocate tty for git on first deployment
add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);
set('allow_anonymous_stats', false);
set('env', 'prod');

set('env_vars', 'SYMFONY_ENV={{env}}');

// Symfony shared dirs
set('shared_dirs', ['var/logs', 'var/sessions']);
// Symfony writable dirs
set('writable_dirs', ['var/cache', 'var/logs', 'var/sessions']);
// Symfony executable and variable directories
set('bin_dir', 'bin');
set('var_dir', 'var');

// Symfony console bin
set('bin/console', function () {
    return sprintf('{{release_path}}/%s/console', trim(get('bin_dir'), '/'));
});


set('dump_assets', true);


// Tasks

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'database:migrate');

after('deploy:assetic:dump', 'deploy:copy_dirs');


task('my_deploy', [
        'deploy:prepare',
        'deploy:lock',
        'deploy:release',
        'deploy:update_code',
        'deploy:shared',
        'deploy:writable',
        'composer:install',
        'deploy:assets:install',
        'gulp:build',
        'deploy:copy_dirs',
        'deploy:clear_paths',
        'deploy:symlink',
        'deploy:unlock',
        'cleanup',
        'success',
    ]
);

task('gulp:build', function () {
    run('cd {{release_path}} && npm install');
    run('cd {{release_path}} && node_modules/.bin/gulp --theme='.get('theming'));
});

task('composer:install', function () {
    run('cd {{release_path}} && SYMFONY_ENV={{env}} {{bin/composer}} {{ composer_options }}', ['tty' => true]);
});
