<?php

namespace App\Controller;


use App\Entity\Customer;
use App\Entity\Invoice;
use App\Entity\InvoiceBase;
use App\Entity\InvoiceItem;
use App\Entity\Project;
use App\Helpers\DataTables;
use App\Repository\TimeReportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class AjaxController
 * @package App\Controller
 */
class AjaxController extends AbstractController
{
    /**
     * @Route("/ajaxRoute/removeContact", name="ajax_remove_contact", methods={"post"})
     * @param Request $request
     * @return JsonResponse
     */
    public function removeContact(Request $request)
    {
        $customerId = $request->request->get('customer');
        $contactId = $request->request->get('contact');

        if (!empty($contactId)) {
            $em = $this->getDoctrine()->getManager();
            $customerContact = $em->getRepository('App:CustomerContact')->find($contactId);
            $em->remove($customerContact);
            $em->flush();
            return new JsonResponse(['success' => true, 'element' => 'contact_' . $contactId]);
        }

        return new JsonResponse(['success' => false]);

    }

    /**
     * @Route("/ajaxRoute/findCustomer", name="ajax_find_customer")
     * @param Request $request
     * @return JsonResponse
     */
    public function findCustomer(Request $request)
    {
        $search = $request->request->get('customer');
        if (!empty($search)) {
            $em = $this->getDoctrine()->getManager();
            $customers = $em->getRepository('App:Customer')->searchCustomer($search);
            /**
             * @var  $key
             * @var Customer $customer
             */
            foreach ($customers as $key => $customer) {
                $customers[$key] = $customer->getCompanyName();
            }

            return new JsonResponse(['success' => true, 'customers' => $customers]);
        }
        return new JsonResponse(['success' => false]);
    }


    /**
     * @Route("/ajaxRoute/findProject", name="ajax_find_project")
     * @param Request $request
     * @return JsonResponse
     */
    public function findProject(Request $request)
    {
        $search = $request->request->get('project');

        if (!empty($search)) {
            $em = $this->getDoctrine()->getManager();
            $projects = $em->getRepository('App:Project')->searchProject($search);
            /**
             * @var  $key
             * @var  Project $project
             */
            foreach ($projects as $key => $project) {
                $projects[$key] = $project->getName();
            }

            return new JsonResponse(['success' => true, 'projects' => $projects]);
        }
        return new JsonResponse(['success' => false]);
    }

    /**
     * @Route("/ajaxRoute/findAllReports", name="ajax_find_reports")
     * @param Request $request
     * @return JsonResponse
     */
    public function findAllReports(Request $request)
    {
        $draw = (int)$request->query->get('draw');

        $columns = [
            [
                'db' => 'id',
                'dt' => 0,
                'formatter' => function ($d, $row) {
                    return '<a href="' . $this->generateUrl('timereport_edit', ['id' => $d]) . '">' . $d . '</a>';
                }
            ],
            [
                'db' => 'project',
                'dt' => 1,
            ],
            [
                'db' => 'createdAt',
                'dt' => 2,
                'formatter' => function ($d, $row) {
                    return $d->format('Y-m-d');
                }
            ],
            ['db' => 'title', 'dt' => 3],
            ['db' => 'description', 'dt' => 4],
            ['db' => 'quantity', 'dt' => 5],
            [
                'db' => 'updatedAt',
                'dt' => 6,
                'formatter' => function ($d, $row) {
                    return $d->format('Y-m-d h:s');
                }
            ],
            [
                'db' => 'id',
                'dt' => 7,
                'formatter' => function ($d, $row) {
                    return '<input class="js-checkbox"
                               value= ' . $d . '
                               type="checkbox" />';
                }
            ],
        ];

        $limit = DataTables::limit($request->query->all(), $columns);
        $order = DataTables::order($request->query->all(), $columns);
        $where = DataTables::filter($request->query->all(), $columns, []);

        /** @var TimeReportRepository $timeReportRepository */
        $timeReportRepository = $this->getDoctrine()
            ->getRepository('App:TimeReport');

        $timeReports = $timeReportRepository->findAllTimeReports($limit, $order, $where);

        $response = [
            'draw' => intval($draw),
            'data' => DataTables::dataOutput($columns, $timeReports['rows']),
            'recordsTotal' => $timeReportRepository->count([]),
            'recordsFiltered' => $timeReports['total'],
        ];

        return new JsonResponse($response);
    }

    /**
     * @Route("/ajaxRoute/findAllInvoiceBases", name="ajax_find_invoice_bases")
     * @param Request $request
     * @return JsonResponse
     */
    public function findAllInvoiceBases(Request $request)
    {
        $draw = (int)$request->query->get('draw');

        $columns = [
            [
                'db' => 'id',
                'dt' => 0,
            ],
            [
                'db' => 'description',
                'dt' => 1
            ],
            [
                'db' => 'quantity',
                'dt' => 2
            ],
            [
                'db' => 'invoice',
                'dt' => 3,
                'formatter' => function ($d, $row) {
                    if (!empty($d)) {
                        return '<a href="' .
                            $this->generateUrl('edit_invoice', ['id' => $d]) . '">' . $d . '</a>';
                    } else {
                        return
                            '<input class="js-checkbox-invoice" id="new-invoice- '. $row['id'] .'"
                               value="' . $row['id'] . '"
                               type="checkbox" />';
                    }
                }

            ],
            [
                'db' => 'project',
                'dt' => 4
            ],

        ];

        $invoiceBaseRepository =
            $this->getDoctrine()
                ->getRepository('App:InvoiceBase');

        $limit = DataTables::limit($request->query->all(), $columns);
        $order = DataTables::order($request->query->all(), $columns);
        $where = DataTables::filter($request->query->all(), $columns, []);

        $result = $invoiceBaseRepository->findAllInvoiceBases($limit, $order, $where);
        $invoiceBases = $result['rows'];


        $response = [
            'draw' => intval($draw),
            'data' => DataTables::dataOutput($columns, $invoiceBases),
            'recordsTotal' => count($invoiceBases),
            'recordsFiltered' => $result['total'],
        ];

        return new JsonResponse($response);
    }

    /**
     * @Route("/ajaxRoute/creteInvoiceBase", name="ajax_create_invoice_base")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createInvoiceBase(Request $request)
    {

        $url = $this->generateUrl('create_invoice_base', $request->get('input'));

        return new JsonResponse(['success' => true, 'url' => $url]);
    }

    /**
     * @Route("/ajaxRoute/createInvoice")
     * @param Request $request
     * @return JsonResponse
     */
    public function createInvoice(Request $request)
    {
        $args['invoiceBaseIds'] = $request->get('invoiceBaseIds');
        $args['invoice'] = $request->get('invoice');

        $url = $this->generateUrl('create_invoice', $args);

        return new JsonResponse(['success' => true, 'url' => $url]);
    }

    /**
     * @Route("/ajaxRoute/setInvoicePaid")
     * @param Request $request
     * @return JsonResponse
     */
    public function setInvoicePaid(Request $request)
    {
        $invoiceId = $request->get('invoiceId');

        $invoiceRepository = $this->getDoctrine()->getRepository('App:Invoice');
        /** @var Invoice $invoice */
        $invoice = $invoiceRepository->find($invoiceId);
        $invoice->setPayStatus(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($invoice);
        $em->flush();
        $invoice = $invoiceRepository->find($invoiceId);
        if ($invoice->getPayStatus()) {
            return new JsonResponse(['success' => true, 'id' => $invoiceId]);
        } else {
            return new JsonResponse(['success' => false]);
        }
    }
}
