<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\CustomerContact;
use App\Form\CustomerType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CustomerController
 * @package App\Controller
 */
class CustomerController extends AbstractController
{
    /**
     * @Route("/customers",name="customers")
     * @return Response
     */
    public function index(AuthorizationCheckerInterface $authorizationChecker)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $customerRepository = $this->getDoctrine()->getRepository('App:Customer');

        $customers = $customerRepository->findAll();

        return $this->render('customer/index.html.twig', ['customers' => $customers]);
    }

    /**
     * @Route("/customers/new", name="customer_new")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     */
    public function newCustomer(Request $request, AuthorizationCheckerInterface $authorizationChecker)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $customer = new Customer();

        $customerContact = new CustomerContact();

        $customer->setCustomerContact($customerContact);

        $form = $this->createForm(CustomerType::class, $customer, [
            'label' => 'customer.button_create',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Customer $customer */
            $customer = $form->getData();
            $customerContact->setCustomer($customer);
            $em->persist($customer);
            $em->persist($customerContact);
            $em->flush();
            return $this->redirectToRoute('_default');
        }


        return $this->render('customer/customerNew.html.twig',['form' => $form->createView()]);
    }

    /**
     * @Route("/customers/edit/{id}", name="customer_edit")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     */
    public function editCustomer(Request $request, AuthorizationCheckerInterface $authorizationChecker, $id)
    {
        $customerRepository = $this->getDoctrine()->getRepository('App:Customer');

        $customer = $customerRepository->find($id);

        $form = $this->createForm(CustomerType::class, $customer, [
            'label' => 'customer.button_save',
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Customer $customer */
            $customer = $form->getData();
            $contacts = [];
            /** @var CustomerContact $contact */
            foreach ($customer->getCustomerContact()->getValues() as $contact) {
                $contact->setCustomer($customer);
                $contact->setCustomerId($customer->getId());
                $customer->removeCustomerContact($contact);
                $customer->setCustomerContact($contact);
            }
            $em->persist($customer);
            $em->flush();
            return $this->redirectToRoute('customers');
        }

        return $this->render('customer/edit.html.twig', ['form' => $form->createView(), 'id' => $id]);
    }
}
