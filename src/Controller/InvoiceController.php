<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Invoice;
use App\Entity\InvoiceBase;
use App\Entity\InvoiceBaseTimeReport;
use App\Entity\InvoiceItem;
use App\Entity\TimeReport;
use App\Entity\User;
use App\Form\InvoiceBaseType;
use App\Form\InvoiceForm;
use App\Form\SelectInvoiceItemType;
use App\Helpers\InvoiceNumber;
use App\Repository\InvoiceBaseRepository;
use App\Repository\InvoiceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class InvoiceController
 * @package App\Controller
 */
class InvoiceController extends AbstractController
{
    /**
     * @Route("/invoice/createInvoiceBase", name="create_invoice_base")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     * @throws \Exception
     */
    public function createInvoiceBase(Request $request, AuthorizationCheckerInterface $authorizationChecker)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }
        $timeReportIds = [];
        parse_str($request->getQueryString(), $timeReportIds);

        $timereportRepository = $this->getDoctrine()->getRepository('App:TimeReport');

        $timeReports = [];
        $quantity = 0;
        foreach ($timeReportIds as $timeReportId) {
            $timeReport = $timereportRepository->find($timeReportId);
            $quantity += $timeReport->getQuantity();
            $timeReports[] = $timeReport;
        }

        $invoiceBase = new InvoiceBase();
        $invoiceBase->setQuantity($quantity);

        $form = $this->createForm(InvoiceBaseType::class, $invoiceBase, [
            'label' => 'invoice.invoicebase.form.button_create',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var InvoiceBase $invoiceBase */
            $invoiceBase = $form->getData();
            $invoiceBase->setCreatedAt(new \DateTime('now'));
            $invoiceBase->setUpdatedAt(new \DateTime('now'));
            $em->persist($invoiceBase);
            $em->flush();
            $invoiceBaseId = $invoiceBase->getId();
            foreach ($timeReports as $timeReport) {
                $invoiceBaseTimeReport = new InvoiceBaseTimeReport($invoiceBaseId, $timeReport->getId());
                $em->persist($invoiceBaseTimeReport);
                $em->flush();
            }

            return $this->redirectToRoute('view_invoice_bases');
        }

        return $this->render('invoice/bases/create.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/invoice/bases", name="view_invoice_bases")
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     */
    public function viewInvoiceBase(AuthorizationCheckerInterface $authorizationChecker)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $invoiceBaseRepository = $this->getDoctrine()->getRepository('App:InvoiceBase');

        $invoiceBases = $invoiceBaseRepository->findAll();

        $result = [];
        /** @var InvoiceBase $invoiceBase */
        foreach ($invoiceBases as $invoiceBase) {
            /** @var TimeReport $timeReport */
            $timeReport = ($invoiceBase->getTimereport())->current();
            $result[$invoiceBase->getId()]['invoiceBase'] = $invoiceBase;
            $result[$invoiceBase->getId()]['projectName'] = $timeReport->getProject()->getName();
        }

        return $this->render('invoice/bases/view.html.twig', ['invoiceBases' => $result]);
    }

    /**
     * @Route("/invoices/createInvoice", name="create_invoice")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param InvoiceBaseRepository $invoiceBaseRepository
     * @return Response
     * @throws \Exception
     */
    public function createInvoice(
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker,
        InvoiceBaseRepository $invoiceBaseRepository,
        InvoiceRepository $invoiceRepository
    )
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }
        $query = [];
        parse_str($request->getQueryString(), $query);

        $quantity = 0;
        $invoiceItem = new InvoiceItem();
        $invoiceBase = null;
        foreach ($query['invoiceBaseIds'] as $invoiceBaseId) {
            /** @var InvoiceBase $invoiceBase */
            $invoiceBase = $invoiceBaseRepository->find($invoiceBaseId);
            $quantity += $invoiceBase->getQuantity();
            $invoiceItem->addInvoiceBase($invoiceBase);
        }

        $invoiceItem->setQuantity($quantity);
        $invoiceItem->setCreatedAt(new \DateTime('now'));
        $invoiceItem->setUpdatedAt(new \DateTime('now'));

        //if no existing invoice then create else add.
        if ($query['invoice'] == -1) {
            $invoice = new Invoice();
            $invoice->setCreatedAt(new \DateTime('now'));
        } else {
            $invoice = $invoiceRepository->find($query['invoice']);
        }

        $invoice->setUpdatedAt(new \DateTime('now'));

        $invoice->addInvoiceItem($invoiceItem);

        $form = $this->createForm(InvoiceForm::class, $invoice, [
            'label' => 'Create invoice',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Invoice $invoice */
            $invoice = $form->getData();
            $invoice->setIsOpen(!$invoice->IsOpen());
            $invoiceNumber = new InvoiceNumber($invoiceRepository);
            $invoiceNumber = $invoiceNumber->createInvoiceNumber();
            $invoice->setInvoiceNumber($invoiceNumber);
            /** @var User $user */
            $user = $this->getUser();
            $invoice->setUser($user);
            $invoice->setUid($user->getId());
            $em->persist($invoice);
            $em->flush();

            return $this->redirectToRoute('view_invoices');
        }

        $timeReport = $invoiceBase->getTimereport()->first();
        $customer = $timeReport->getProject()->getCustomer();

        return $this->render('invoice/create.html.twig',
            [
                'form' => $form->createView(),
                'customer' => $customer,
            ]
        );
    }

    /**
     * @Route("invoices/", name="view_invoices")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param InvoiceRepository $invoiceRepository
     * @return Response
     */
    public function viewInvoices(
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker,
        InvoiceRepository $invoiceRepository
    )
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $result = $invoiceRepository->findBy([], ['id' => 'desc']);
        $invoices = [];

        $i = 0;
        /** @var Invoice $invoice */
        foreach ($result as $invoice) {
            $timeReport = null;

            $amount = 0;
            $amountVat = 0;
            /** @var InvoiceBase $invoiceBase */
            $invoiceBase = null;
            /** @var InvoiceItem $invoiceItem */
            foreach ($invoice->getInvoiceItems() as $invoiceItem) {
                $price = $invoiceItem->getPrice() * $invoiceItem->getQuantity();
                $amount += $price;
                $amountVat += $price * ($invoiceItem->getVat() / 100);
                $invoiceBases = $invoiceItem->getInvoiceBase()->getValues();
                if (!empty($invoiceBases)) {
                    $invoiceBase = current($invoiceBases);
                }
            }
            $timeReport = $invoiceBase->getTimereport()->first();

            $invoices[$i]['invoice'] = $invoice;
            $invoices[$i]['amount']['no_vat'] = $amount;
            $invoices[$i]['amount']['vat'] = $amountVat;
            $invoices[$i]['amount']['total'] = $amount + $amountVat;

            $invoices[$i]['company']['name'] = $timeReport->getProject()->getCompanyName();
            $invoices[$i]['company']['id'] = $timeReport->getProject()->getCustomerId();
            $invoices[$i]['project']['name'] = $timeReport->getProjectName();
            $invoices[$i++]['project']['id'] = $timeReport->getProjectId();
        }

        return $this->render('invoice/view.html.twig', ['invoices' => $invoices]);
    }

    /**
     * @Route("/invoice/{id}", name="edit_invoice")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param InvoiceRepository $invoiceRepository
     * @param int $id
     * @return Response
     */
    public function viewInvoice(
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker,
        InvoiceRepository $invoiceRepository,
        int $id
    )
    {
        /** @var Invoice $invoice */
        $invoice = $invoiceRepository->find($id);

        $form = $this->createForm(InvoiceForm::class, $invoice, [
            'label' => 'invoice.form.button_save',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var Invoice $invoice */
            $invoice = $form->getData();
            /** @var User $user */
            $user = $this->getUser();
            $invoice->setUid($user->getId());

            $em->persist($invoice);
            $em->flush();


        }

        /** @var InvoiceItem $invoiceItem */
        $invoiceItems = $invoice->getInvoiceItems()->getValues();
        $invoiceBase = null;
        foreach ($invoiceItems as $invoiceItem) {
            $invoiceBases = $invoiceItem->getInvoiceBase()->getValues();
            if (!empty($invoiceBases)) {
                $invoiceBase = current($invoiceBases);
            }
        }

        /** @var TimeReport $timeReport */
        $timeReport = $invoiceBase->getTimereport()->first();
        /** @var Customer $customer */
        $customer = $timeReport->getProject()->getCustomer();

        return $this->render('invoice/create.html.twig',
            [
                'form' => $form->createView(),
                'customer' => $customer,
                'invoiceId' => $invoice->getId()
            ]);
    }

    /**
     * @Route("/invoice/print/{id}", name="print_invoice")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param InvoiceRepository $invoiceRepository
     * @param int $id
     * @return Response
     */
    public function printInvoice(
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker,
        InvoiceRepository $invoiceRepository,
        int $id
    )
    {
        /** @var Invoice $invoice */
        $invoice = $invoiceRepository->find($id);

        /** @var InvoiceItem $invoiceItem */
        $invoiceItems = $invoice->getInvoiceItems()->getValues();
        $invoiceBase = null;
        foreach ($invoiceItems as $invoiceItem) {
            $invoiceBases = $invoiceItem->getInvoiceBase()->getValues();
            if (!empty($invoiceBases)) {
                $invoiceBase = current($invoiceBases);
            }
        }

        /** @var TimeReport $timeReport */
        $timeReport = $invoiceBase->getTimereport()->first();
        /** @var Customer $customer */
        $customer = $timeReport->getProject()->getCustomer();
        $contact = current($customer->getCustomerContact()->getValues());
        return $this->render('invoice/printInvoice.html.twig',
            [
                'invoice' => $invoice,
                'items' => $invoiceItems,
                'customer' => $customer,
                'contact' => $contact,
            ]);
    }


    /**
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param InvoiceRepository $invoiceRepository
     * @return Response
     */
    public function selectInvoice(
        Request $request,
        AuthorizationCheckerInterface $authorizationChecker,
        InvoiceRepository $invoiceRepository
    )
    {

        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $result = $invoiceRepository->findBy(['isOpen' => true]);
        $invoices['invoice.form.select.nochoice'] = -1;
        /** @var Invoice $invoice */
        foreach ($result as $invoice) {
            /** @var InvoiceItem $invoiceItem */
            $invoiceItem = current($invoice->getInvoiceItems()->getValues());
            /** @var InvoiceBase $invoiceBase */
            $invoiceBase = current($invoiceItem->getInvoiceBase()->getValues());
            /** @var TimeReport $timeReport */
            $timeReport = current($invoiceBase->getTimereport()->getValues());
            $company = $timeReport->getProject()->getCompanyName();
            $project = $timeReport->getProjectName();

            $invoices[$company . ' - ' . $project] = $invoice->getId();
        }

        $form = $this->createForm(SelectInvoiceItemType::class, [], [
            'invoices' => $invoices,
        ]);
        $response = $this->render('invoice/InvoiceChoice.html.twig', ['form' => $form->createView(),]);

        return $response;
    }
}
