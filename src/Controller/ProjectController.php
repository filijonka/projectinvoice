<?php

namespace App\Controller;


use App\Entity\Customer;
use App\Entity\Project;
use App\Form\ProjectType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Class ProjectController
 * @package App\Controller
 */
class ProjectController extends AbstractController
{

    /**
     * @Route("/projects", name="projects")
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     */
    public function index(AuthorizationCheckerInterface $authorizationChecker)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $projectRepository = $this->getDoctrine()->getRepository('App:Project');

        $projects = $projectRepository->findAll();

        return $this->render('project/index.html.twig', ['projects' => $projects]);

    }


    /**
     * @Route("/projects/new", name="projects_new")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     */
    public function createProject(Request $request, AuthorizationCheckerInterface $authorizationChecker)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $project = new Project();
        $project->setCustomer(new Customer());
        $form = $this->createForm(ProjectType::class, $project, [
            'label' => 'project.form.button.create',
        ]);

        if ($request->request->has($form->getName())) {
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Project $project */
            $project = $form->getData();
            $customer = $project->getCustomer();
            $customerRepository = $this->getDoctrine()->getRepository('App:Customer');
            /** @var Customer $customer */
            $customer = current($customerRepository->findBy(['companyName' => $customer->getCompanyName()]));
            $project->setCustomer($customer);
            $project->setCustomerId($customer->getId());
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();
            return $this->redirectToRoute('_default');

        }

        return $this->render('project/new.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/projects/edit/{id}", name="projects_edit")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param int $id
     * @return Response
     */
    public function editProject(Request $request, AuthorizationCheckerInterface $authorizationChecker, $id)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $projectRepository = $this->getDoctrine()->getRepository('App:Project');

        $project = $projectRepository->find($id);

        $form = $this->createForm(ProjectType::class, $project, [
            'label' => 'project.form.button.save',
        ]);

        if ($request->request->has($form->getName())) {
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $project = $form->getData();
            $customer = $project->getCustomer();
            $customerRepository = $this->getDoctrine()->getRepository('App:Customer');
            /** @var Customer $customer */
            $customer = current($customerRepository->findBy(['companyName' => $customer->getCompanyName()]));
            $project->setCustomerId($customer->getId());
            $project->setCustomer($customer);
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();
            return $this->redirectToRoute('_default');

        }
        $formView = $form->createView();
        return $this->render('project/edit.html.twig', ['form' => $formView]);
    }
}
