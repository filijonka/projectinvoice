<?php

namespace App\Controller;


use App\Entity\Project;
use App\Entity\TimeReport;
use App\Form\TimeReportType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class TimeReportController
 * @package App\Controller
 */
class TimeReportController extends AbstractController
{

    /**
     * @Route("/timereport/add", name="timereport_add")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     * @throws \Exception
     */
    public function createTimeReport(Request $request, AuthorizationCheckerInterface $authorizationChecker)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $timeReport = new TimeReport();
        $timeReport->setProject(new Project());

        $form = $this->createForm(TimeReportType::class, $timeReport, [
            'label' => 'timereport.form.button_create',
        ]);

        if ($request->request->has($form->getName())) {
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            /** @var TimeReport $timeReport */
            $timeReport = $form->getData();
            $projectName = $timeReport->getProject()->getName();
            $projectRepository = $this->getDoctrine()->getRepository('App:Project');
            /** @var Project $project */
            $project = current($projectRepository->findBy(['name' => $projectName]));
            $timeReport->setProject($project);
            $timeReport->setProjectId($project->getId());
            $timeReport->setCreatedAt(new \DateTime('now'));
            $timeReport->setUpdatedAt(new \DateTime('now'));
            $em->persist($timeReport);
            $em->flush();
            return $this->redirectToRoute('timereport_add');
        }

        return $this->render('timereport/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/timereport/edit/{id}", name="timereport_edit")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function editTimeReport(Request $request, AuthorizationCheckerInterface $authorizationChecker, $id)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        $timeReportRepository = $this->getDoctrine()->getRepository('App:TimeReport');

        $timeReport = $timeReportRepository->find($id);

        $form = $this->createForm(TimeReportType::class, $timeReport, [
            'label' => 'timereport.form.button_save',
        ]);

        if ($request->request->has($form->getName())) {
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $timeReport = $form->getData();
            $timeReport->setUpdatedAt(new \DateTime('now'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($timeReport);
            $em->flush();
            return $this->redirectToRoute('timereport_view');
        }
            return $this->render('timereport/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/timereport/view", name="timereport_view")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @return Response
     */
    public function viewtimeReports(Request $request, AuthorizationCheckerInterface $authorizationChecker)
    {
        if (!$authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }

        return $this->render('timereport/view.html.twig');
    }
}
