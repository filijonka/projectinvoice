<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table(name="customer")
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 */
class Customer
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=100, nullable=false)
     */
    protected $companyName;


    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=100, nullable=false)
     */
    protected $street;

    /**
     * @var string
     *
     * @ORM\Column(name="building_number", type="string", length=15, nullable=true)
     */
    protected $buildingNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=25, nullable=false)
     */
    protected $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50, nullable=false)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=50, nullable=true)
     */
    protected $country = 'Sverige';

    /**
     * @var string
     *
     * @ORM\Column(name="vat_number", type="string", length=25, nullable=true)
     */
    protected $vatNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    protected $description;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\CustomerContact", mappedBy="customer", cascade={"persist"}, fetch="EAGER")
     */
    protected $customerContact;


    public function __construct()
    {
        $this->customerContact = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Customer
     */
    public function setId($id): Customer
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string
    {
        return (string) $this->companyName;
    }

    /**
     * @param string $companyName
     * @return Customer
     */
    public function setCompanyName($companyName): Customer
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return (string) $this->street;
    }

    /**
     * @param string $street
     * @return Customer
     */
    public function setStreet($street): Customer
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuildingNumber(): string
    {
        return (int) $this->buildingNumber;
    }

    /**
     * @param string $buildingNumber
     * @return Customer
     */
    public function setBuildingNumber($buildingNumber): Customer
    {
        $this->buildingNumber = $buildingNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipcode(): string
    {
        return (string) $this->zipcode;
    }

    /**
     * @param string $zipcode
     * @return Customer
     */
    public function setZipcode($zipcode): Customer
    {
        $this->zipcode = $zipcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return (string) $this->city;
    }

    /**
     * @param string $city
     * @return Customer
     */
    public function setCity($city): Customer
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return (string) $this->country;
    }

    /**
     * @param string $country
     * @return Customer
     */
    public function setCountry($country): Customer
    {
        $country = empty($country) ? 'Sverige' : $country;
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getVatNumber(): string
    {
        return (string) $this->vatNumber;
    }

    /**
     * @param string $vatNumber
     * @return Customer
     */
    public function setVatNumber($vatNumber): Customer
    {
        $this->vatNumber = $vatNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return (string) $this->description;
    }

    /**
     * @param string $description
     * @return Customer
     */
    public function setDescription($description): Customer
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCustomerContact()
    {
        return  empty($this->customerContact) ? new ArrayCollection() : $this->customerContact;
    }

    /**
     * @param CustomerContact $customerContact
     * @return Customer
     */
    public function setCustomerContact(CustomerContact $customerContact): Customer
    {
        if (empty($this->customerContact)) {
            $this->customerContact = new ArrayCollection([$customerContact]);
        } else {
            $this->customerContact->add($customerContact);
        }
        return $this;
    }

    /**
     * @param CustomerContact $customerContact
     * @return Customer
     */
    public function removeCustomerContact(CustomerContact $customerContact):Customer
    {
        if ($this->customerContact->contains($customerContact)) {
            $this->customerContact->removeElement($customerContact);
        }

        return $this;
    }


}

