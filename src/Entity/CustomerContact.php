<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customer
 *
 * @ORM\Table(name="customer_contact",
 *     indexes={
 *          @ORM\Index(name="FK_customer_id_idx", columns={"customer_id"}),
 *      }
 *     )
 * @ORM\Entity
 */

class CustomerContact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_id", type="integer")
     */
    protected $customerId;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="customerContact", fetch="EAGER")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="customer_id", referencedColumnName="id"),
     * })
     */
    protected $customer;


    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50, nullable=false)
     */
    protected $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50, nullable=false)
     */
    protected $lastname;

    /**
     * @var string
     * @ORM\Column(name="phonenumber", type="string", length=30, nullable=true)
     */
    protected $phone;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCustomerId(): int
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     * @return CustomerContact
     */
    public function setCustomerId(int $customerId): CustomerContact
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer): void
    {
        $this->customer = $customer;
    }


    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return (string) $this->firstname;
    }

    /**
     * @param string $firstname
     * @return CustomerContact
     */
    public function setFirstname(string $firstname): CustomerContact
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return (string) $this->lastname;
    }

    /**
     * @param string $lastname
     * @return CustomerContact
     */
    public function setLastname(string $lastname): CustomerContact
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return (string) $this->phone;
    }

    /**
     * @param string $phone
     * @return CustomerContact
     */
    public function setPhone(string $phone): CustomerContact
    {
        $this->phone = $phone;
        return $this;
    }

}