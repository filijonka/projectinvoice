<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Invoice
 * @package App\Entity
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 */
class Invoice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="invoice_number", type="string", length=8, unique=true)
     */
    protected $invoiceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    protected $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_open", type="boolean", nullable=false, options={"default":true})
     */
    protected $isOpen = true;
    /**
     * @var integer
     *
     * @ORM\Column(name="pay_limit", type="smallint", nullable=false, options={"default": 30})
     */
    protected $payLimit = 30;

    /**
     * @var string
     *
     * @ORM\Column(name="pay_status", type="boolean", nullable=false, options={"default": 0})
     */
    protected $payStatus = 0;

    /**
     * @var integer
     * @ORM\Column(name="uid", type="integer", nullable=false)
     */
    protected $uid;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="uid", referencedColumnName="id"),
     * })
     */
    protected $user;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\InvoiceItem", inversedBy="invoice", cascade={"persist"})
     * @ORM\JoinTable(name="invoice_invoiceitem",
     *   joinColumns={
     *     @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="invoiceitem_id", referencedColumnName="id")
     *   }
     * )
     */
    protected $invoiceItems;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

    public function __construct()
    {
        $this->invoiceItems = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * @param $invoiceNumber
     * @return Invoice
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Invoice
     */
    public function setDescription(string $description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getPayLimit()
    {
        return $this->payLimit;
    }

    /**
     * @param int $payLimit
     * @return Invoice
     */
    public function setPayLimit(int $payLimit)
    {
        $this->payLimit = $payLimit;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayStatus(): string
    {
        return $this->payStatus;
    }

    /**
     * @param string $payStatus
     * @return Invoice
     */
    public function setPayStatus(string $payStatus)
    {
        $this->payStatus = $payStatus;

        return $this;
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     * @return Invoice
     */
    public function setUid(int $uid)
    {
        $this->uid = $uid;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
    /**
     * @return bool
     */
    public function isOpen()
    {
        return $this->isOpen;
    }

    /**
     * @param bool $isOpen
     * @return Invoice
     */
    public function setIsOpen(bool $isOpen)
    {
        $this->isOpen = $isOpen;

        return $this;
    }
    /**
     * @return Collection
     */
    public function getInvoiceItems(): Collection
    {
        return $this->invoiceItems;
    }

    /**
     * @param Collection $invoiceItems
     * @return Invoice
     */
    public function setInvoiceItems(Collection $invoiceItems)
    {
        $this->invoiceItems = $invoiceItems;

        return $this;
    }

    /**
     * @param InvoiceItem $invoiceItem
     * @return Invoice
     */
    public function addInvoiceItem(InvoiceItem $invoiceItem)
    {
        $this->getInvoiceItems()->add($invoiceItem);

        return $this;
    }

    /**
     * @param InvoiceItem $invoiceItem
     * @return Invoice
     */
    public function removeInvoiceItem(InvoiceItem $invoiceItem)
    {
        $this->getInvoiceItems()->removeElement($invoiceItem);

        return $this;
    }
    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $createdAt
     * @return Invoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Invoice
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return Invoice
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

}

