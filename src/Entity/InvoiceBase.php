<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class InvoiceBase
 * @package App\Entity
 *
 * @ORM\Table(name="invoicebase")
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceBaseRepository")
 */
class InvoiceBase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    protected $description;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $quantity;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="TimeReport", inversedBy="invoicebase")
     * @ORM\JoinTable(name="invoicebase_timereport",
     *   joinColumns={
     *     @ORM\JoinColumn(name="invoicebase_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="timereport_id", referencedColumnName="id")
     *   }
     * )
     */
    protected $timereport;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\InvoiceItem", inversedBy="invoiceBase")
     * @ORM\JoinTable(name="invoicebase_invoiceitem",
     *   joinColumns={
     *     @ORM\JoinColumn(name="invoicebase_id", referencedColumnName="id", unique=true)
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="invoiceitem_id", referencedColumnName="id")
     *   }
     * )
     */
    protected $invoiceItem;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->timereport = new ArrayCollection();
        $this->invoiceItem = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return InvoiceBase
     */
    public function setId(int $id): InvoiceBase
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return (string) $this->description;
    }

    /**
     * @param string $description
     * @return InvoiceBase
     */
    public function setDescription(string $description): InvoiceBase
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     * @return InvoiceBase
     */
    public function setQuantity($quantity): InvoiceBase
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTimereport(): Collection
    {
        return $this->timereport;
    }

    /**
     * @param Collection $timereport
     * @return InvoiceBase
     */
    public function setTimereport(\Doctrine\Common\Collections\Collection $timereport): InvoiceBase
    {
        $this->timereport = $timereport;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getInvoiceItem(): Collection
    {
        return $this->invoiceItem;
    }

    /**
     * @param Collection $invoiceItem
     * @return InvoiceBase
     */
    public function setInvoiceItem(Collection $invoiceItem)
    {
        $this->invoiceItem = $invoiceItem;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $createdAt
     * @return InvoiceBase
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return InvoiceBase
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return InvoiceBase
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

}
