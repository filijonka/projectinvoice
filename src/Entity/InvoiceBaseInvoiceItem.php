<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InvoiceBaseInvoiceItem
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="invoicebase_invoiceitem")
 */
class InvoiceBaseInvoiceItem
{

    /**
     * @var integer
     * @ORM\Id @ORM\Column(name="invoicebase_id", type="integer")
     */
    protected $invoiceBaseId;

    /**
     * @var integer
     * @ORM\Id @ORM\Column(name="invoiceitem_id", type="integer")
     */
    protected $invoiceitemId;

    public function __construct($invoiceBaseId, $invoiceitemId)
    {
        $this->invoiceBaseId = $invoiceBaseId;
        $this->invoiceitemId = $invoiceitemId;
    }

    /**
     * @return int
     */
    public function getInvoiceBaseId()
    {
        return $this->invoiceBaseId;
    }

    /**
     * @param int $invoiceBaseId
     * @return InvoiceBaseInvoiceItem
     */
    public function setInvoiceBaseId(int $invoiceBaseId)
    {
        $this->invoiceBaseId = $invoiceBaseId;

        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceitemId()
    {
        return $this->invoiceitemId;
    }

    /**
     * @param int $invoiceitemId
     * @return InvoiceBaseInvoiceItem
     */
    public function setInvoiceitemId(int $invoiceitemId)
    {
        $this->invoiceitemId = $invoiceitemId;

        return $this;
    }

}