<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="invoicebase_timereport")
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceBaseTimeReportRepository")
 */
class InvoiceBaseTimeReport
{
    /**
     * @var integer
     * @ORM\Id @ORM\Column(name="invoicebase_id", type="integer")
     */
    protected $invoiceBaseId;

    /**
     * @var integer
     * @ORM\Id @ORM\Column(name="timereport_id", type="integer")
     */
    protected $timeReportId;

    public function __construct($invoiceBaseId, $timeReportId)
    {
        $this->invoiceBaseId = $invoiceBaseId;
        $this->timeReportId = $timeReportId;
    }

    /**
     * @return int
     */
    public function getInvoiceBaseId()
    {
        return $this->invoiceBaseId;
    }

    /**
     * @param int $invoiceBaseId
     * @return InvoiceBaseTimeReport
     */
    public function setInvoiceBaseId(int $invoiceBaseId)
    {
        $this->invoiceBaseId = $invoiceBaseId;

        return $this;
    }

    /**
     * @return int
     */
    public function getTimeReportId()
    {
        return $this->timeReportId;
    }

    /**
     * @param int $timeReportId
     * @return InvoiceBaseTimeReport
     */
    public function setTimeReportId(int $timeReportId)
    {
        $this->timeReportId = $timeReportId;

        return $this;
    }


}
