<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InvoiceBaseInvoiceItem
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="invoice_invoiceitem")
 */
class InvoiceInvoiceItem
{

    /**
     * @var integer
     * @ORM\Id @ORM\Column(name="invoice_id", type="integer")
     */
    protected $invoiceId;

    /**
     * @var integer
     * @ORM\Id @ORM\Column(name="invoiceitem_id", type="integer")
     */
    protected $invoiceitemId;

    public function __construct($invoiceId, $invoiceitemId)
    {
        $this->invoiceId = $invoiceId;
        $this->invoiceitemId = $invoiceitemId;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     * @return InvoiceInvoiceItem
     */
    public function setInvoiceId(int $invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceitemId()
    {
        return $this->invoiceitemId;
    }

    /**
     * @param int $invoiceitemId
     * @return InvoiceInvoiceItem
     */
    public function setInvoiceitemId(int $invoiceitemId)
    {
        $this->invoiceitemId = $invoiceitemId;

        return $this;
    }

}