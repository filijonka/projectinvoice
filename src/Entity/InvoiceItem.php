<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class InvoiceItem
 * @package App\Entity
 *
 * @ORM\Table(name="invoice_item")
 * @ORM\Entity()
 */
class InvoiceItem
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    protected $description;

    /**
     * @var float
     *
     * @ORM\Column(name="quantity", type="decimal", precision=10, scale=2, nullable=false)
     */
    protected $quantity;


    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=false)
     */
    protected $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="vat", type="integer", nullable=false, options={"default":25})
     */
    protected $vat = 25;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\InvoiceBase", mappedBy="invoiceItem")
     *
     */
    protected $invoiceBase;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Invoice", mappedBy="invoiceItems")
     *
     */
    protected $invoice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

    public function __construct()
    {
        $this->invoiceBase = new ArrayCollection();
        $this->invoice = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return InvoiceItem
     */
    public function setDescription(string $description): InvoiceItem
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     * @return InvoiceItem
     */
    public function setQuantity($quantity): InvoiceItem
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return InvoiceItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getVat(): int
    {
        return $this->vat;
    }

    /**
     * @param int $vat
     * @return InvoiceItem
     */
    public function setVat(int $vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getInvoiceBase(): Collection
    {
        return $this->invoiceBase;
    }

    /**
     * @param Collection $invoiceBase
     */
    public function setInvoiceBase(Collection $invoiceBase): void
    {
        $this->invoiceBase = $invoiceBase;
    }

    /**
     * @param InvoiceBase $invoicebase
     * @return InvoiceItem
     */
    public function addInvoiceBase(InvoiceBase $invoicebase)
    {
        $this->getInvoiceBase()->add($invoicebase);

        return $this;
    }

    /**
     * @param InvoiceBase $invoiceBase
     * @return InvoiceItem
     */
    public function removeInvoiceBase(InvoiceBase $invoiceBase)
    {
        $this->getInvoiceBase()->removeElement($invoiceBase);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getInvoice(): Collection
    {
        return $this->invoice;
    }

    /**
     * @param Collection $invoice
     * @return InvoiceItem
     */
    public function setInvoice(Collection $invoice)
    {
        $this->invoice = $invoice;

        return $this;
    }
    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return InvoiceItem
     */
    public function setCreatedAt(\DateTime $createdAt): InvoiceItem
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return InvoiceItem
     */
    public function setUpdatedAt(\DateTime $updatedAt): InvoiceItem
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt(): \DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     * @return InvoiceItem
     */
    public function setDeletedAt(\DateTime $deletedAt): InvoiceItem
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

}
