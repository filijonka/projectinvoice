<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Validator as CustomerConstraint;

/**
 * Class Project
 * @package App\Entity
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 *
 * @CustomerConstraint\IsCustomer()
 */
class Project
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=100, nullable=false, unique=true)
     */
    protected $name;


    /**
     * @var integer
     * @ORM\Column(name="customer_id", type="integer", nullable=false)
     */
    protected $customerId;

    /**
     * @var Customer
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer")
     */
    protected $customer;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customerId
     * @return Project
     */
    public function setCustomerId(int $customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param $customer
     * @return Project
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->customer->getCompanyName();
    }

    public function setCompanyName($companyName)
    {
        $this->customer->setCompanyName($companyName);
    }
}
