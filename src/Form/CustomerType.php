<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyName', TextType::class, [
                'label' => 'customer.form.label_companyname',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('street', TextType::class, [
                'label' => 'customer.form.label_street',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('buildingNumber', IntegerType::class, [
                'label' => 'customer.form.label_buildingnr',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('zipcode', TextType::class, [
                'label' => 'customer.form.label_zipcode',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('city', TextType::class, [
                'label' => 'customer.form.label_postal',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('country', TextType::class, [
                'label' => 'customer.form.label_country',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('vatNumber', TextType::class, [
                'label' => 'customer.form.label_vat',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('description', TextareaType::class, [
                'label' => 'customer.form.label_description',
                'required' => false,
                'empty_data' => '',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('customerContact', CollectionType::class, [
                'entry_type' => CustomerContactType::class,
                'allow_add' => true,
                'label' => 'customer.contact',
            ])
            ->add('save',
                SubmitType::class,
                [
                    'label' => $options['label'],
                    'attr' => [
                        'class' => 'btn btn-primary'
                    ],
                ]
            );
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Customer',
            'translation_domain' => 'customer',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'letitrock_invoicebundle_customer';
    }


}
