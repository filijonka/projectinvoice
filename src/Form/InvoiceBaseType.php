<?php


namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InvoiceBaseType
 * @package App\Form
 */
class InvoiceBaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'invoice.invoicebase.form.label_description'
            ])
            ->add('quantity', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'invoice.invoicebase.form.label_quantity',
            ])
            ->add('save',
                SubmitType::class,
                [
                    'label' => $options['label'],
                    'attr' => [
                        'class' => 'btn btn-primary'
                    ],
                ]
            );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\InvoiceBase',
            'translation_domain' => 'invoice',
        ]);
    }

    public function getBlockPrefix()
    {
        return 'letitrock_invoice_invoice_base';
    }

}