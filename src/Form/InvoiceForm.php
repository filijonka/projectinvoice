<?php


namespace App\Form;


use App\Entity\Invoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class InvoiceForm
 * @package App\Form
 */
class InvoiceForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('invoiceitems', CollectionType::class, [
                'entry_type' => InvoiceItemType::class,
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('isOpen', CheckboxType::class, [
                'label' => 'invoice.form.checkbox_close',
                'required' => false,
                'attr' => [
                    'class' => 'form-control',
                ],
                'data' => false,
             ])
            ->add('description', TextareaType::class, [
                'label' => 'invoice.form.label_description',
                'attr' => [
                    'class' => 'form-control',
                ],
                'required' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'invoice.form.button_save',
                'attr' => [
                    'class' => 'btn btn-primary js-save-invoice',
                    'required' => false,
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
            'translation_domain' => 'invoice',
        ]);

    }

    public function getBlockPrefix()
    {
        return 'letitrock_invoice_invoice';
    }

}