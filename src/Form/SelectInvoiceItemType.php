<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SelectInvoiceItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', ChoiceType::class, [
                    'label' => false,
                    'choices' => $options['invoices'],
                    'attr' => [
                        'class' => 'form-control js-select-invoice'
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'invoices' => null,
            'translation_domain' => 'invoice',
        ]);

    }

    public function getBlockPrefix()
    {
        return 'letitrock_sprangkonsult_bundle_invoice';
    }
}
