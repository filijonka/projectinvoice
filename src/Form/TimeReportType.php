<?php

namespace App\Form;

use App\Repository\ProjectRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TimeReportType
 * @package App\Form
 */
class TimeReportType extends AbstractType
{
    protected $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'timereport.form.label_title',
            ])
            ->add('description', TextareaType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'timereport.form.label_description',
            ])
            ->add('quantity', IntegerType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'label' => 'timereport.form.label_quantity',
            ])
            ->add('createdAt', DateType::class, [
                    'attr' => [
                        'class' => 'form-control',
                    ],
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                    'label' => 'timereport.form.label_date',
                ]
            )
            ->add(
                $builder
                    ->create('project', TextType::class, [
                    'attr' => [
                        'class' => 'form-control',
                    ],
                    'label' => 'timereport.form.label_projectname',
                    ]
                    )->addModelTransformer(
                    new CallbackTransformer(
                        function ($project) {
                            return $project->getName();
                        },
                        function ($projectName) {
                            $project = current($this->projectRepository->searchProject($projectName));
                            return $project;
                        }
                    )
                )
            )
            ->add('save',
                SubmitType::class,
                [
                    'label' => $options['label'],
                    'attr' => [
                        'class' => 'btn btn-primary'
                    ],
                ]
            );

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\TimeReport',
            'translation_domain' => 'timereporting',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'letitrock_timereport';
    }

}
