<?php

namespace App\Helpers;


use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use Doctrine\ORM\EntityManagerInterface;

class InvoiceNumber
{
    protected $invoiceRepository;

    public function __construct(InvoiceRepository $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;

    }

    public function createInvoiceNumber()
    {
        /** @var Invoice $prevInvoice */
        $prevInvoice = current($this->invoiceRepository->findBy([],['id' => 'desc'],1));
        $prevInvoiceNumber = $prevInvoice->getInvoiceNumber();
        $number = (int) str_split($prevInvoiceNumber,4)[1] +1;
        $number = str_split((string) $number);
        $length = count($number)-1;

        $newNumber = [];

        for ($i = 3; $i > -1;$i--) {
            if (isset($number[$length])) {
                $newNumber[$i] = $number[$length];
                $length--;
            } else {
                $newNumber[$i] = 0;
            }
        }
        sort($newNumber);
        $number = implode($newNumber);
        $currentYear = date('Y', time());

        return $currentYear . $number;
    }

}