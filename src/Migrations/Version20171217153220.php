<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171217153220 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customer (
                id INT AUTO_INCREMENT NOT NULL,
                company_name VARCHAR(100) NOT NULL,
                street VARCHAR(100) NOT NULL,
                building_number VARCHAR(15) DEFAULT NULL,
                zipcode VARCHAR(25) NOT NULL,
                city VARCHAR(50) NOT NULL,
                country VARCHAR(50) DEFAULT NULL,
                vat_number VARCHAR(25) DEFAULT NULL,
                description TEXT DEFAULT NULL,
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE customer_contact (
                id INT AUTO_INCREMENT NOT NULL,
                customer_id INT NOT NULL,
                firstname VARCHAR(50) NOT NULL,
                lastname VARCHAR(50) NOT NULL,
                phonenumber VARCHAR(30),
                PRIMARY KEY(id),
                INDEX `fk_customer_id_idx` (`customer_id` ASC),
                CONSTRAINT `fk_customer_id` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE user (
                id INT AUTO_INCREMENT NOT NULL,
                username VARCHAR(25) NOT NULL,
                email VARCHAR(100) NOT NULL,
                password VARCHAR(72) NOT NULL,
                role VARCHAR(32) NOT NULL,
                UNIQUE INDEX UNIQ_8D93D649F85E0677 (username),
                PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql(
            'CREATE TABLE invoice ' .
            '( ' .
                'id INT AUTO_INCREMENT NOT NULL, ' .
                'description TEXT DEFAULT NULL, ' .
                'pay_limit SMALLINT DEFAULT 30 NOT NULL, ' .
                'pay_status TINYINT(1) DEFAULT \'0\' NOT NULL, ' .
                'uid INT NOT NULL, ' .
                'created_at DATETIME NOT NULL, ' .
                'updated_at DATETIME NOT NULL, ' .
                'deleted_at DATETIME DEFAULT NULL, ' .
                'is_open TINYINT(1) DEFAULT \'1\' NOT NULL, ' .
                'PRIMARY KEY(id), ' .
                'CONSTRAINT IDX_90651744539B0606 FOREIGN KEY (uid) REFERENCES user(id) ' .
            ') DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;'
        );

        $this->addSql('CREATE TABLE project (
                id INT AUTO_INCREMENT NOT NULL,
                name VARCHAR(100) NOT NULL,
                customer_id INT NOT NULL,
                PRIMARY KEY(id),
                INDEX `UNIQ_2FB3D0EE5E237E06` (`name` ASC)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ENGINE = InnoDB');

        $this->addSql('CREATE TABLE timereport (
                id INT AUTO_INCREMENT NOT NULL,
                quantity NUMERIC(10, 2) NOT NULL,
                description TEXT NOT NULL,
                title VARCHAR(100) NOT NULL,
                PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('INSERT INTO `user` (`id`,`username`,`email`,`password`,`role`) VALUES (1,\'peppe\',\'peter@letitrock.se\',\'$2y$12$pMDHyzUxXSliKQEfQr1.9.tgnp9V1IgN808GyTW4CN79Gjs2ux0TO\',\'ROLE_USER\');');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE timereport');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE customer_contact');
        $this->addSql('DROP TABLE customer');
    }
}
