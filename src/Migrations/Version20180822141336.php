<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20180822141336
 * @package DoctrineMigrations
 */
final class Version20180822141336 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql(
            'CREATE TABLE invoicebase ' .
            ' (' .
                'id INT AUTO_INCREMENT NOT NULL, ' .
                'quantity  NUMERIC(10, 2) NOT NULL, '.
                'description TEXT NOT NULL, ' .
                'created_at DATETIME NOT NULL, ' .
                'updated_at DATETIME NOT NULL, ' .
                'deleted_at DATETIME NULL, ' .
                'PRIMARY KEY(id)' .
            ') '.
            'DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;'
        );

        $this->addSql(
            'CREATE TABLE invoice_item ' .
            '( ' .
            'id INT AUTO_INCREMENT NOT NULL, ' .
            'description TEXT NOT NULL, ' .
            'quantity NUMERIC(10, 2) NOT NULL, ' .
            'price INT NOT NULL, ' .
            'vat INT DEFAULT 25 NOT NULL, ' .
            'created_at DATETIME NOT NULL, ' .
            'updated_at DATETIME NOT NULL, ' .
            'deleted_at DATETIME DEFAULT NULL, ' .
            'PRIMARY KEY(id) ' .
            ') DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;'
        );


        $this->addSql(
            'CREATE TABLE invoicebase_invoiceitem ' .
            '( '.
            'invoicebase_id INT NOT NULL, '.
            'invoiceitem_id INT NOT NULL, '.
            'INDEX IDX_CA9BED7E2B62A591 (invoicebase_id), ' .
            'INDEX IDX_CA9BED7E506A288E (invoiceitem_id), ' .
            'PRIMARY KEY(invoicebase_id, invoiceitem_id), '.
            'CONSTRAINT FK_CA9BED7E2B62A591 FOREIGN KEY (invoicebase_id) REFERENCES invoicebase (id), ' .
            'CONSTRAINT FK_CA9BED7E506A288E FOREIGN KEY (invoiceitem_id) REFERENCES invoice_item (id) ' .
            ') DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;'
        );

        $this->addSql(
            'CREATE TABLE invoice_invoiceitem ' .
            '(' .
                'invoice_id INT NOT NULL, '.
                'invoiceitem_id INT NOT NULL, ' .
                'INDEX IDX_EDCF31142989F1FD (invoice_id), ' .
                'INDEX IDX_EDCF3114506A288E (invoiceitem_id), '.
                'PRIMARY KEY(invoice_id, invoiceitem_id), '.
                'CONSTRAINT FK_EDCF31142989F1FD FOREIGN KEY (invoice_id) REFERENCES invoice (id), '.
                'CONSTRAINT FK_EDCF3114506A288E FOREIGN KEY (invoiceitem_id) REFERENCES invoice_item (id) '.
            ') DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;'
        );

        $this->addSql(
            'CREATE TABLE invoicebase_timereport ' .
            '(' .
                'invoicebase_id INT NOT NULL, ' .
                'timereport_id INT NOT NULL, ' .
                'INDEX IDX_A424CA8B2B62A591 (invoicebase_id), ' .
                'UNIQUE INDEX UNIQ_A424CA8BC197A26C (timereport_id), ' .
                'PRIMARY KEY(invoicebase_id, timereport_id) ' .
            ') ' .
            'DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB'
        );

        $this->addSql(
            'ALTER TABLE invoicebase_timereport ' .
                    'ADD CONSTRAINT FK_A424CA8B2B62A591 FOREIGN KEY (invoicebase_id) REFERENCES invoicebase (id)'
        );

        $this->addSql(
            'ALTER TABLE invoicebase_timereport ' .
                    'ADD CONSTRAINT FK_A424CA8BC197A26C FOREIGN KEY (timereport_id) REFERENCES timereport (id)'
        );

        $this->addSql('ALTER TABLE timereport ADD deleted_at DATETIME DEFAULT NULL;');

        $this->addSql(
            'ALTER TABLE invoicebase_invoiceitem ADD UNIQUE INDEX `invoicebase_id_UNIQUE` (`invoicebase_id` ASC);'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE invoicebase_timereport');
        $this->addSql('DROP TABLE invoicebase_invoiceitem');
        $this->addSql('DROP TABLE invoicebase');
        $this->addSql('DROP TABLE invoice_invoiceitem');
        $this->addSql('DROP TABLE invoice_item');
        $this->addSql('DROP TABLE invoice');
        $this->addSql('DROP TABLE user');

        $this->addSql('ALTER TABLE timereport DROP COLUMN deleted_at;');
    }
}
