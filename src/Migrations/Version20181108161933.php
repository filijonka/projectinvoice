<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181108161933
 * @package DoctrineMigrations
 *
 * This is for a migration from drupal 7 with the invoice module
 */
final class Version20181108161933 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        //fetch and insert all customers
        $this->addSql(
            'INSERT INTO customer (id, company_name, street, building_number, zipcode, city, country,vat_number) ' .
            'SELECT customer_number as id, company_name, any_value(street) as street, ' .
                   'any_value(building_number) as building_number, any_value(zipcode) as zipcode, ' .
                   'any_value(city) as city, ' .
                   'IF(any_value(country) != \'\', any_value(country), \'Sverige\') as country, '.
                    'vat_number ' .
            'FROM letitrock.invoice_customers ' .
            'GROUP BY id, company_name, vat_number ' .
            'ORDER BY id;'
        );

        //Add all contacts on customers if there exist one
        $this->addSql(
            'INSERT INTO customer_contact (customer_id, firstname, lastname) ' .
            'SELECT c.id as customer_id, firstname, any_value(lastname) ' .
            'FROM letitrock.invoice_customers ic  ' .
            'JOIN customer c ON c.id = ic.customer_number ' .
            'WHERE firstname != \'\' ' .
            'GROUP BY firstname, customer_id ' .
            'ORDER by customer_number; '
        );

        $this->addSql(
            'INSERT INTO project (name, customer_id) ' .
            'SELECT field_project_value AS name, any_value(ic.customer_number) AS customer_id ' .
            'FROM letitrock.invoice_invoices ii ' .
            'JOIN letitrock.invoice_customers ic ON ic.invoice_id = ii.iid ' .
            'JOIN letitrock.node n ON ii.nid = n.nid ' .
            'JOIN letitrock.field_data_field_invoice_id field_invoice ON n.title LIKE concat(\'%\',field_invoice.field_invoice_id_value, \'%\')' .
            'JOIN letitrock.field_data_field_project field_project ON field_project.entity_id=field_invoice.entity_id ' .
            'GROUP BY name;'
        );

        //we need to copy the original tables into temporary so we can manipulate the data
        $this->addSql('CREATE TABLE test.node SELECT * FROM letitrock.node;');

        $this->addSql('CREATE TABLE test.field_data_field_date SELECT * FROM letitrock.field_data_field_date;');

        $this->addSql('CREATE TABLE test.field_data_field_description SELECT * FROM letitrock.field_data_field_description;');

        $this->addSql('CREATE TABLE test.field_data_field_hours SELECT * FROM letitrock.field_data_field_hours;');

        $this->addSql('CREATE TABLE test.field_data_field_project SELECT * FROM letitrock.field_data_field_project;');

        $this->addSql('CREATE TABLE test.field_data_field_invoice_id SELECT * FROM letitrock.field_data_field_invoice_id;');

        //correct the data
        $this->addSql(
            'UPDATE test.field_data_field_project ' .
            'SET field_project_value=\'sprangkonsult\' ' .
            'WHERE field_project_value=\'Sprängkonsult\';');
        $this->addSql(
            'UPDATE test.field_data_field_project ' .
            'SET field_project_value=\'Tule Trafikskola\' ' .
            'WHERE entity_id=146;');

        $this->addSql(
            'UPDATE test.field_data_field_project ' .
            'SET field_project_value=\'Musikcentrum\' ' .
            'WHERE entity_id=147;');

        $this->addSql(
            'UPDATE test.field_data_field_project ' .
            'SET field_project_value=\'symfony_sprangkonsult\' ' .
            'WHERE entity_id=512;');

        //get all timereports
        $this->addSql(
            'INSERT INTO timereport (id, quantity, description, title, project_id, createdAt, updatedAt) ' .
            'SELECT ' .
            'n.nid as id, ' .
            'field_hours.field_hours_value as quantity, ' .
            'IF (ISNULL(field_description.field_description_value), \'ingen beskrivning\', field_description.field_description_value) AS description, ' .
            'n.title, ' .
            'p.id as project_id, ' .
            'FROM_UNIXTIME(n.created) as createdAt, ' .
            'FROM_UNIXTIME(n.changed) as updatedAt ' .
            'FROM test.node n ' .
            'LEFT JOIN letitrock.field_data_field_invoice_id iid ON iid.entity_id = n.nid ' .
            'JOIN test.field_data_field_date field_date ON field_date.entity_id = n.nid ' .
            'LEFT JOIN test.field_data_field_description field_description ON field_description.entity_id = n.nid ' .
            'JOIN test.field_data_field_hours field_hours ON field_hours.entity_id = n.nid ' .
            'JOIN test.field_data_field_project as field_project ON field_project.entity_id = n.nid ' .
            'JOIN project p ON lower(p.name) like lower(field_project.field_project_value) ' .
            'where n.type=\'tidsrapport\' AND (iid.field_invoice_id_value != -1 or iid.field_invoice_id_value is null) ' .
            'ORDER BY nid;'
        );
        //drop the tables
        $this->addSql('DROP TABLE test.node');
        $this->addSql('DROP TABLE test.field_data_field_date');
        $this->addSql('DROP TABLE test.field_data_field_description');
        $this->addSql('DROP TABLE test.field_data_field_hours');
        $this->addSql('DROP TABLE test.field_data_field_project');
        $this->addSql('DROP TABLE test.field_data_field_invoice_id');

        //add invoices
        $this->addSql('INSERT INTO invoice (id,description,pay_limit,pay_status, uid, created_at, updated_at, is_open, invoice_number) ' .
            'SELECT DISTINCT items.invoice_id, ii.description, ii.pay_limit, IF(ii.pay_status = "paid", 1, 0) as pay_status, 1, ' .
            'FROM_UNIXTIME(n.created) AS created_at, ' .
            'FROM_UNIXTIME(n.changed) AS updated_at, ' .
            'IF(status = 1, 0,1) as is_open, ' .
            'substring(title, 10) as invoice_number ' .
            'FROM letitrock.node n ' .
            'JOIN letitrock.invoice_invoices ii ON ii.nid= n.nid ' .
            'JOIN letitrock.invoice_items items ON ii.iid = items.invoice_id ' .
            'where n.type = "invoice";'
        );

        /*** INSERT into invoicebase and connect them with a timereport ***/
        $this->addSql('

        DROP PROCEDURE IF EXISTS _create_invoice_bases;

        CREATE PROCEDURE _create_invoice_bases()
        BEGIN
            DECLARE cursor_List_isdone BOOLEAN DEFAULT FALSE;
            DECLARE quantity decimal(10,2);
            DECLARE description text;
            DECLARE createdAt, updatedAt datetime;
            DECLARE timereport_id int;

            DECLARE cursor_List CURSOR FOR 
                SELECT fh.field_hours_value as quantity, 
                    IF(fd.field_description_value is null, \'Ingen beskrivning\', fd.field_description_value )  as description, 
                    FROM_UNIXTIME(n.created) as createdAt, FROM_UNIXTIME(n.changed) as updatedAt,
                    t.id as timereport_id
                FROM letitrock.node n
                LEFT JOIN letitrock.field_data_field_invoice_id iid ON iid.entity_id = n.nid
                JOIN letitrockinvoice.invoice i ON strcmp(i.invoice_number, iid.field_invoice_id_value) = 0
                JOIN letitrock.invoice_items it ON it.invoice_id = i.id
                LEFT JOIN letitrock.field_data_field_description fd ON fd.entity_id = n.nid
                JOIN letitrock.field_data_field_hours fh ON fh.entity_id = n.nid
                JOIN letitrockinvoice.timereport t ON  t.id = n.nid
                WHERE n.type = \'tidsrapport\' AND (iid.field_invoice_id_value != -1 or iid.field_invoice_id_value is null)
                GROUP BY n.nid, fh.field_hours_value, fd.field_description_value, n.created, n.changed,
                    t.id
            ;

            DECLARE CONTINUE HANDLER FOR NOT FOUND SET cursor_List_isdone = TRUE;

            OPEN cursor_List;

            loop_List: LOOP
                FETCH cursor_List INTO quantity, description, createdAt, updatedAt, timereport_id;
                IF cursor_List_isdone THEN
                LEAVE loop_List;
                END IF;
        
                INSERT INTO letitrockinvoice.invoicebase (quantity, description, created_at, updated_at) 
                VALUES (quantity, description, createdAt, updatedAt);
        
                INSERT INTO letitrockinvoice.invoicebase_timereport (invoicebase_id, timereport_id) 
                VALUES (LAST_INSERT_ID(), timereport_id);
        
            END LOOP loop_List;

            CLOSE cursor_List;
        END;
        ');

        $this->addSql('CALL _create_invoice_bases();');

        $this->addSql('
            DROP PROCEDURE IF EXISTS _tmp_create_invoice_items;
            
            CREATE PROCEDURE _tmp_create_invoice_items()
            BEGIN
                
                DECLARE cursor_List_isdone BOOLEAN DEFAULT FALSE;
                DECLARE quantity decimal(10,2);
                DECLARE description text;
                DECLARE price int;
                DECLARE vat int;
                DECLARE createdAt datetime;
                DECLARE invoiceitem_id, invoice_id int;
                        
                DECLARE cursor_List CURSOR FOR 
                    SELECT it.iid AS invoiceitem_id, it.description, it.quantity, it.unitcost as price, it.vat, FROM_UNIXTIME(it.created), it.invoice_id
                    FROM letitrock.invoice_items it
                    JOIN letitrock.invoice_invoices i ON i.iid = it.invoice_id
                ;
                    
                DECLARE CONTINUE HANDLER FOR NOT FOUND SET cursor_List_isdone = TRUE;
                    
                OPEN cursor_List;
                    
                loop_List: LOOP
                    FETCH cursor_List INTO invoiceitem_id, description, quantity, price, vat, createdAt, invoice_id;
                    IF cursor_List_isdone THEN
                        LEAVE loop_List;
                    END IF;

                    INSERT INTO letitrockinvoice.invoice_item (id, description, quantity, price, vat, created_at, updated_at)
                    VALUES (invoiceitem_id, description,quantity, price, vat, createdAt, createdAt);

                    INSERT INTO letitrockinvoice.invoice_invoiceitem (invoice_id, invoiceitem_id)
                        VALUES (invoice_id, invoiceitem_id);

                END LOOP loop_List;
                
                CLOSE cursor_List;
            END;'
        );

        $this->addSql('CALL _tmp_create_invoice_items()');

        $this->addSql('
            DROP PROCEDURE IF EXISTS _tmp_create_invoicebase_invoiceitem;
                
            CREATE PROCEDURE _tmp_create_invoicebase_invoiceitem()
            
                BEGIN
                    DECLARE cursor_List_isdone BOOLEAN DEFAULT FALSE;
                    DECLARE invoiceitem_id, invoicebase_id int;
                            
                    DECLARE cursor_List CURSOR FOR 
                            SELECT ib.id as invoicebase_id, it.iid as invoiceitem_id
                            FROM letitrock.node n
                            JOIN letitrock.field_data_field_invoice_id iid ON iid.entity_id = n.nid
                            JOIN letitrockinvoice.invoice i ON strcmp(i.invoice_number, iid.field_invoice_id_value) = 0
                            JOIN letitrock.invoice_items it ON it.invoice_id = i.id
                            JOIN letitrock.field_data_field_description fd ON fd.entity_id = n.nid
                            JOIN letitrock.field_data_field_hours fh ON fh.entity_id = n.nid
                            JOIN letitrockinvoice.timereport t ON  t.id = n.nid
                            JOIN letitrockinvoice.invoicebase_timereport ibt ON ibt.timereport_id = t.id
                            JOIN letitrockinvoice.invoicebase ib ON ib.id = ibt.invoicebase_id
                            WHERE n.type = \'tidsrapport\'
                            ORDER BY invoicebase_id
                            ;
                        
                    DECLARE CONTINUE HANDLER FOR NOT FOUND SET cursor_List_isdone = TRUE;
                        
                    OPEN cursor_List;
                        
                    loop_List: LOOP
                        FETCH cursor_List INTO invoicebase_id, invoiceitem_id;
                        IF cursor_List_isdone THEN
                           LEAVE loop_List;
                        END IF;
                        
                        INSERT IGNORE INTO letitrockinvoice.invoicebase_invoiceitem (invoicebase_id, invoiceitem_id)
                                VALUES (invoicebase_id, invoiceitem_id);
                                            
                    END LOOP loop_List;
                        
                    CLOSE cursor_List;
            END;
        ');

        $this->addSql('CALL _tmp_create_invoicebase_invoiceitem()');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM invoicebase_timereport');
        $this->addSql('DELETE FROM invoicebase_invoiceitem');
        $this->addSql('DELETE FROM invoicebase');
        $this->addSql('DELETE FROM timereport');
        $this->addSql('DELETE FROM project');
        $this->addSql('DELETE FROM customer_contact');
        $this->addSql('DELETE FROM customer');

    }
}
