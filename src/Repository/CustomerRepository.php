<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
/**
 * CustomerRepository
 *
 */
class CustomerRepository extends EntityRepository
{
    public function searchCustomer(string $customer)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $like = $qb->expr()->like('c.companyName', "'$customer%'");
        $result = $qb->select('c')
            ->from('App:Customer', 'c')
            ->orderBy('c.id', 'asc')
            ->where($like)
            ->getQuery()
            ->getResult();

        return $result;

    }
}
