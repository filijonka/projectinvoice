<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class InvoiceBaseRepository
 * @package App\Repository
 */
class InvoiceBaseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, 'App:InvoiceBase');
    }

    public function findAllInvoiceBases(array $limit, array $order, array $where): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $select = [
            'ib.id',
            'ib.description',
            'ib.quantity',
            'p.name as project',
            'c.companyName as customer',
            'i.id as invoice'
        ];

        $qb->select($select)
            ->from('App:InvoiceBase', 'ib')
            ->join(
                'App\Entity\InvoiceBaseTimeReport',
                'ibt',
                Expr\Join::WITH,
                'ibt.invoiceBaseId = ib.id'
            )
            ->join(
                'App\Entity\Timereport',
                't',
                Expr\Join::WITH,
                't.id = ibt.timeReportId'
            )
            ->join(
                'App\Entity\Project',
                'p',
                Expr\Join::WITH,
                't.project = p.id')
            ->join(
                'App\Entity\Customer',
                'c',
                Expr\Join::WITH,
                'c.id = p.customerId'
            )
            ->leftjoin(
                'App\Entity\InvoiceBaseInvoiceItem',
                'ibit',
                Expr\Join::WITH,
                'ibit.invoiceBaseId = ib.id'
            )
            ->leftjoin(
                'App\Entity\InvoiceItem',
                'ii',
                Expr\Join::WITH,
                'ii.id = ibit.invoiceitemId'
            )
            ->leftjoin(
                'App\Entity\InvoiceInvoiceItem',
                'iit',
                Expr\Join::WITH,
                'iit.invoiceitemId = ii.id'
            )
            ->leftjoin(
                'App\Entity\Invoice',
                'i',
                Expr\Join::WITH,
                'iit.invoiceId = i.id'
            );

        $orderColumn = '';
        switch ($order['column']) {
            case 'project':
                $orderColumn = 'p'.$order['column'];
                break;
            case 'invoice':
                $orderColumn = 'i.id';
                break;
            default:
                $orderColumn = 'ib.'.$order['column'];
        }

        $qb->addGroupBy('ib.id, ib.description, ib.quantity, p.name, c.companyName, i.id');

        $queryTotal = $qb->orderBy($orderColumn, $order['direction']);
        $total = count($queryTotal->getQuery()->getArrayResult());

        $qb->orderBy($orderColumn, $order['direction'])
            ->setFirstResult($limit['start']);
        if (isset($limit['length'])) {
            $qb->setMaxResults($limit['length']);
        }


        $query = $qb->getQuery();

        $result['rows'] = $query->getArrayResult();
        $result['total'] = $total;

        return $result;
    }

}
