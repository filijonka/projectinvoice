<?php

namespace App\Repository;

use App\Entity\InvoiceBaseTimeReport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InvoiceBaseTimeReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvoiceBaseTimeReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvoiceBaseTimeReport[]    findAll()
 * @method InvoiceBaseTimeReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceBaseTimeReportRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InvoiceBaseTimeReport::class);
    }

//    /**
//     * @return InvoiceBaseTimeReport[] Returns an array of InvoiceBaseTimeReport objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InvoiceBaseTimeReport
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
