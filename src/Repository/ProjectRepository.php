<?php

namespace App\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ProjectRepository
 * @package App\Repository
 */
class ProjectRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, 'App:Project');
    }


    /**
     * @param string $projectname
     * @return mixed
     */
    public function searchProject(string $projectname)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $like = $qb->expr()->like('p.name', "'$projectname%'");
        $result = $qb->select('p')
            ->from('App:Project', 'p')
            ->orderBy('p.id', 'asc')
            ->where($like)
            ->getQuery()
            ->getResult();

        return $result;

    }

}