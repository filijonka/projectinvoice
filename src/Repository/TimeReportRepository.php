<?php

namespace App\Repository;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

/**
 * Class TimeReportRepository
 * @package App\Repository
 */
class TimeReportRepository extends EntityRepository
{


    public function findAllTimeReports(array $limit, array $order, array $where): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $select = [
            't.id',
            'p.name AS project',
            't.createdAt',
            't.title',
            't.description',
            't.quantity',
            't.updatedAt'
        ];

        $qb->select($select)
            ->from('App:TimeReport', 't')
            ->join('App:Project',
                'p',
                Expr\Join::WITH,
                't.project = p.id');

        $subQb = $this->getEntityManager()->createQueryBuilder();
        $timeReportIds =
            $subQb
            ->select(['ibtr.timeReportId'])
            ->from('App:InvoiceBaseTimeReport', 'ibtr')
            ->getQuery()
            ->getArrayResult();

        $timeReportIds = array_column($timeReportIds, 'timeReportId');

        if (!empty($timeReportIds)) {
            $timeReports = $qb->expr()->notIn('t.id', $timeReportIds);

            $qb->andWhere($timeReports);
        }

        //@todo should be placed in a AND statement
        foreach ($where as $key => $value) {
            if (!is_numeric($value)) {
                switch ($key) {
                    case 'id':
                        break;
                    case 'createdAt':
                    case 'updatedAt':
                        try {
                            new \DateTime($value);
                            $qb->orWhere($qb->expr()->like('t.' . $key, ':' . $key));
                            $qb->setParameter($key, '%' . $value . '%');
                        } catch (\Exception $e) {
                            //we aren't going to do anything
                        }
                        break;
                    case 'project';
                        $qb->orWhere($qb->expr()->like('p.name', ':' . $key));
                        $qb->setParameter($key, '%' . $value . '%');
                        break;
                    default:
                        $qb->orWhere($qb->expr()->like('t.' . $key, ':' . $key));
                        $qb->setParameter($key, '%' . $value . '%');
                        break;
                }
            } else {
                switch ($key) {
                    case 'id':
                        $qb->orWhere($qb->expr()->like('t.' . $key, ':' . $key));
                        $qb->setParameter($key, '%' . $value . '%');
                        break;
                    case 'createdAt':
                    case 'updatedAt':
                        try {
                            new \DateTime($value);
                            $qb->orWhere($qb->expr()->like('t.' . $key, ':' . $key));
                            $qb->setParameter($key, '%' . $value . '%');
                        } catch (\Exception $e) {
                            //we aren't going to do anything
                        }
                        break;
                }

            }
        }

        $queryTotal = $qb->orderBy('t.'.$order['column'], $order['direction']);

        $total = count($queryTotal->getQuery()->getArrayResult());
        $qb->orderBy('t.'.$order['column'], $order['direction'])
            ->setFirstResult($limit['start']);
        if (isset($limit['length'])) {
            $qb->setMaxResults($limit['length']);
        }

        $query = $qb->getQuery();

        $result['rows'] = $query->getArrayResult();
        $result['total'] = $total;

        return $result;

    }
}
