<?php

namespace App\Validator;

use App\Entity\Customer;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

use Doctrine\ORM\EntityManagerInterface;

class IsCustomerValidator extends ConstraintValidator
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($project, Constraint $constraint)
    {
        $customerRepository = $this->entityManager->getRepository('App:Customer');
        /** @var Customer $customer */
        $customer = $project->getCustomer();
        $id = current($customerRepository->findBy(['companyName' => $customer->getCompanyName()]));
        if (!$id) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $customer->getCompanyName())
                ->atPath('projects_new')
                ->addViolation();
        }
    }
}
