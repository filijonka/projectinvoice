<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsProject extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'The project {{value}} do not exist';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
