<?php

namespace App\Validator;

use App\Entity\Project;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

use Doctrine\ORM\EntityManagerInterface;

class IsProjectValidator extends ConstraintValidator
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($timeReport, Constraint $constraint)
    {
        $projectRepository = $this->entityManager->getRepository('App:Project');
        /** @var Project $project */
        $project = $timeReport->getProject();
        $id = current($projectRepository->findBy(['name' => $project->getName()]));
        if (!$id) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $project->getName())
                ->addViolation();
        }
    }
}
